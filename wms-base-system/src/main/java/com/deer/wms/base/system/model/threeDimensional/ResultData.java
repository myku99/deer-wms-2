package com.deer.wms.base.system.model.threeDimensional;

public class ResultData {
    private Integer total;
    private Object data;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
