package ${basePackage}.model;

import com.deer.wms.common.core.service.QueryCriteria;

/**
* Created by ${author} on ${date}.
*/
public class ${modelNameUpperCamel}Criteria extends QueryCriteria {
}
